import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')

);

// const header=React.createElement("H1",null,"Header");
//
// const par=React.createElement("ul",null,[React.createElement("li",null,"VanillA"),React.createElement("li",null,"Chocolate")]);
// const main=React.createElement("div",null,[header,par]);
// ReactDOM.render(main,document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
