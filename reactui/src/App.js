import logo from './logo.svg';
import './App.css';


import {BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom';
import Search from "./components/Search";
import AddPayment from "./components/AddPayment";
import {Button, ButtonGroup, Navbar, NavbarBrand} from "reactstrap";
import FindAll from "./components/FindAll";
import logoimg from './allstate-logo.png'

function App() {

  return (
    <div className="App">
      {/*<header className="App-header">*/}
      {/*  <img src={logo} className="App-logo" alt="logo" />*/}
      {/*  <p>*/}
      {/* This is my first react app*/}
      {/*  </p>*/}

      {/*</header>*/}
      {/*  <Greet> Dee</Greet>*/}

      {/*  <UserContactList name={"All State"}> </UserContactList>*/}
      {/*  <UserContactTable name={"All State"}> </UserContactTable>*/}

        <div style={{"background":"linear-gradient(to bottom, #0020 , turquoise)"}}>

            <h1 style={{"color":"white"}}>Payment Service</h1>
        </div>
        <Router>

            {/*style={{background:'green'}}*/}
<div style={{"display":"flex" ,"justifyContent":"center","background":"whitesmoke"} }>
    <img src={logoimg} tag={Link} to={"/" } />


                <Button class="btn"  size="sm" color="primary" tag={Link} to={"/" }>PaymentsList</Button>
    <Button class="btn"  size="sm" color="primary" tag={Link} to={"/search" }>Search</Button>
</div>

            <Switch>
                <Route path='/' exact={true} component={FindAll}/>
                <Route path='/Add' exact={true} component={AddPayment}/>
                <Route path='/search' component={Search}/>
                {/*<Button size="sm" color="primary" tag={Link} to={"/Add" }>Add</Button>*/}


                {/*/!*<div>*!/*/}
                {/*/!*<Button size="sm" color="primary" tag={Link} to={"/Home" }>Payments</Button>*!/*/}
                {/*/!*</div>*/}

            </Switch>

        </Router>
        {/*<CustomerStatus> </CustomerStatus>*/}
        {/*<CustomerTable></CustomerTable>*/}
        {/*<AddCustomer></AddCustomer>*/}
    </div>
  );
}

export default App;
