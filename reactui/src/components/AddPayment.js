import React, {Component} from 'react';
import axios from "axios";
import {Button, Form, FormFeedback, FormGroup, FormText, Input, Label} from "reactstrap";
import {Link, Switch} from "react-router-dom";


export default class AddPayment extends Component {

    constructor(props) {
        super(props);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onChangeCustId = this.onChangeCustId.bind(this);

        this.saveCustomer = this.saveCustomer.bind(this);

        this.state = {
            id: 25,

            type:"",
            amount:"",
            custid:"",
            submitted: false
        };
    }


    onChangeType(e) {
        this.setState({
            type: e.target.value
        });
    }
    onChangeAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }
    onChangeCustId(e) {
        this.setState({
            custid: e.target.value
        });
    }



    saveCustomer() {
        var data = {
            id:this.state.id+1,
            type: this.state.type,
            amount: this.state.amount ,
            custid:this.state.custid}
        axios.post('http://localhost:8080/api/payment/save', data)
            .then(response => {
                this.setState({submitted: response.data.id})
                alert("payment saved");
                this.setState({
                    type: "",
                    amount: "",
                    custid: ""

                })
            } )
            .catch(e => {
                alert(e);
                console.log(e);
            });




    }

    render() {
        return (
            <div style={{"marginTop":"6%"}}>
            <h3 style={{"color":"white"}}>Add Payment</h3>

            <div style={{"width":"80%",  "margin": "0 auto","border":"1px solid white" ,"marginTop":"4%"}} >

            <Form style={{"border":"30px"}}>
                <FormGroup>
                    <Label for="exampleEmail">Type</Label>
                    <Input id={"type"} value={this.state.type} style={{"width":"25%", "marginLeft":"38%"}} onChange={this.onChangeType} />


                </FormGroup>
                <FormGroup>
                    <Label for="exampleEmail">Amount</Label>
                    <Input value={this.state.amount} style={{"width":"25%", "marginLeft":"38%"}} onChange={this.onChangeAmount} />


                </FormGroup>
                <FormGroup>
                    <Label for="examplePassword">CusomerID</Label>
                    <Input value={this.state.custid} style={{"width":"25%", "marginLeft":"38%"}} onChange={this.onChangeCustId} />


                </FormGroup>
                <Button onClick={this.saveCustomer}>Submit</Button>

            </Form>
            </div>
            </div>
        //     <div>
        //         {/*<div>*/}
        //         {/*<Button size="sm" color="primary" tag={Link} to={"/Home" }>Payments</Button>*/}
        //         {/*</div>*/}
        //
        // <form>
        //     <h1>Add Customer</h1>
        //     <div className="form-group">
        //         <label htmlFor="txtname"> Name</label>
        //         <input type="text" className="form-control" id="txtname" aria-describedby="fnameHelp"
        //                placeholder="Enter name" value={this.state.name} onChange={this.onChangeName}/>
        //
        //     </div>
        //
        //
        //     <div className="form-group">
        //         <label htmlFor="txtline1"> Name</label>
        //         <input type="text" className="form-control" id="txtline1" aria-describedby="fnameHelp"
        //                placeholder="Enter address" value={this.state.line1} onChange={this.onChangeLine1}/>
        //
        //     </div>
        //
        //     <div className="form-group">
        //         <label htmlFor="exampleid">Id</label>
        //         <input type="number" className="form-control" id="exampleid" aria-describedby="emailid"
        //                placeholder="Enter id" value={this.state.id} onChange={this.onChangeId}/>
        //     </div>
        //
        //
        //     <div className="form-group">
        //         <input type="submit" className="form-control"  onClick={this.saveCustomer}/>
        //     </div>
        //
        // </form>
        //     </div>)
        ) }
}

