import React, {Component} from 'react';
import axios from "axios";
import {Button, Form, FormGroup, Input, Label, Table} from 'reactstrap';
import {Link, Switch} from "react-router-dom";


export default class FindAll extends Component {

    constructor(props) {
        super(props);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onChangeCustId = this.onChangeCustId.bind(this);
        this.onChangeId = this.onChangeId.bind(this);
        this.saveCustomer = this.saveCustomer.bind(this);
        this.show=this.show.bind(this);

        this.state = {
            contactList: [],
            count: 0,
            add:false,
            id: "",
            type:"",
            amount:"",
            custid:"",
            submitted: false

        }
    }

    onChangeType(e) {
        this.setState({
            type: e.target.value
        });
    }
    onChangeAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }
    onChangeCustId(e) {
        this.setState({
            custid: e.target.value
        });
    }
    onChangeId(e) {
        this.setState({
            id: e.target.value
        });
    }

    componentDidMount() {
        this.getPayments();
    }

    getPayments(){
        console.log("Am i coming here");
        axios
            .get("http://localhost:8080/api/payment/findAll")
            .then(response => {
                // create an array of contacts only with relevant data
                console.log(response.data[0]);
                const newContacts = response.data.map(c => {
                    return {
                        id: c.id,
                        date: c.paymentdate,
                        type: c.type,
                        amount:c.amount,
                        custid:c.custid
                    };
                });
                this.setState({contactList: newContacts})
                console.log(newContacts);
            })

            .catch(function (error) {
                console.log(error);
            });


    }
    show(){

        this.setState({add:true});


    }
    saveCustomer() {

        var data = {
            id: this.state.id ,
            type: this.state.type,
            amount: this.state.amount,
            custid: this.state.custid
        }
        axios.post('http://localhost:8080/api/payment/save', data)
            .then(response => {
                //this.setState({submitted: response.data.id})
                this.getPayments();
                alert("payment saved");
                this.setState({
                    id:"",
                    type: "",
                    amount: "",
                    custid: ""


                })
            })
            .catch(e => {
                alert(e);
                console.log(e);
            });

    }


        render()
        {

            return (
                <div style={{"marginTop": "6%"}}>

                    <h3 style={{"color": "white"}}> PaymentsList</h3>
                    <Button style={{"marginTop": "-6%","marginLeft":"60%"}} class="btn" size="sm" color="primary" onClick={this.show}>AddPayment</Button>
                    <div style={{"width": "60%","marginLeft":"5%", "marginTop": "4%"}}>


                        <Table striped bordered style={{"background": "white"}}>
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Date</th>
                                <th>Type</th>
                                <th>Amount</th>
                                <th>CustomerId</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.contactList.map(d => (<tr>
                                <td>{d.id}</td>
                                <td>{d.date}</td>
                                <td>{d.type}</td>
                                <td>{d.amount}</td>
                                <td>{d.custid}</td>

                            </tr>))}
                            </tbody>
                        </Table>
                    </div>


                    {this.state.add == true &&


                        // <div style={{
                        //     "width": "80%",
                        //     "margin": "0 auto",
                        //     "border": "1px solid white",
                        //     "marginTop": "4%"
                        // }}>"marginTop":"15%",marginLeft:"0%" ,
<div style={{"width":"20%", position:"relative",marginTop:"-58%",marginLeft:"70%" }}>
                            <Form style={{"marginTop":"0%",marginLeft:"2%"}}>
                                <FormGroup>
                                    <Label for="exampleEmail">PaymentID</Label>
                                    <Input id={"type"} value={this.state.id}
                                           onChange={this.onChangeId}/>


                                </FormGroup>
                                <FormGroup>
                                    <Label for="exampleEmail">Type</Label>
                                    <Input id={"type"} value={this.state.type}
                                           onChange={this.onChangeType}/>


                                </FormGroup>
                                <FormGroup>
                                    <Label for="exampleEmail">Amount</Label>
                                    <Input value={this.state.amount}
                                           onChange={this.onChangeAmount}/>


                                </FormGroup>
                                <FormGroup>
                                    <Label for="examplePassword">CusomerID</Label>
                                    <Input value={this.state.custid}
                                           onChange={this.onChangeCustId}/>


                                </FormGroup>
                                <Button onClick={this.saveCustomer}>Submit</Button>

                            </Form>
</div>
                    //
                    // </div>
                    //
                            }
                </div>);


        }
    }