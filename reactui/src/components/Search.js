import React, {Component} from 'react';
import axios from "axios";
import {Button, Form, FormGroup, Input, Label, Row, Table} from "reactstrap";



export default class Search extends Component {

    constructor(props) {
        super(props);


        this.onChangeId=this.onChangeId.bind(this);
        this.onSubmitfind= this.onSubmitfind.bind(this);
        this.state = {
            contactList: [],
            count: '',
            paymentid:'',
            id:'',
            paymentdate:"",
            type:"",
            amount:'',
            custid:''
        };
    }

    onChangeId(e) {
        console.log("ID coming------------")
        this.setState({
            paymentid: e.target.value
        });
        console.log(this.state.paymentid)
    }
   async onSubmitfind(e) {
       // e.preventDefault();
        var id=this.state.paymentid;
        console.log(this.state.paymentid)
        console.log("Am i coming here");
        axios
            .get("http://localhost:8080/api/payment/find/"+id)

            .then(response => {
                // create an array of contacts only with relevant data
                console.log(response);
              this.setState({

                  id:response.data.id,
                  paymentdate:response.data.paymentdate,
                  type:response.data.type,
                  amount:response.data.amount,
                  custid:response.data.custid
              })



            })

            .catch(function (error) {
                alert("Payment not found");
                console.log(error);
            });

    }

    render() {
        return (
            <div style={{"marginTop":"6%"}}>
                <h3 style={{"color":"white"}}>Payment Finder</h3>

                <div style={{"width":"80%",  "margin": "0 auto","marginTop":"4%"}} >
<div>
            <Form>
                <Row form>
                <FormGroup inline>
                    <Label for="examplePassword" style={{"color":"white"}}>PaymentID</Label>
                    <Input style={{"width":"30%","marginLeft":"35%"}}   value={this.state.paymentid}   id="paymentid" placeholder="search payment" onChange={this.onChangeId} />

                    </FormGroup>
                <Button onClick={this.onSubmitfind}>Submit</Button>
                </Row>
            </Form>
</div>
            <Table striped bordered hover style={{"background":"white"}}>
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Amount</th>
                    <th>CustomerId</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{this.state.id}</td>
                    <td>{this.state.paymentdate}</td>
                    <td>{this.state.type}</td>
                    <td>{this.state.amount}</td>
                    <td>{this.state.custid}</td>

                </tr>
                </tbody>
            </Table>
        </div>
            </div>

        );
    }
}

